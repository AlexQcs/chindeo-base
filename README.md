# ChindeoBase
###1.  Project build.gradle
```groovy
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```
###2. Module build.gradle
```groovy
    implementation 'com.gitee.AlexQcs.chindeo-base:lazy:1.0.4'
    implementation 'com.gitee.AlexQcs.chindeo-base:final:1.0.4'
    implementation 'com.gitee.AlexQcs.chindeo-base:repostory:1.0.4'
    implementation 'com.gitee.AlexQcs.chindeo-base:language:1.0.4'
```
