package com.chindeo.skin.option.executor;

import static com.chindeo.skin.option.executor.ShadowLayoutExecutorBuilder.ATTRIBUTE_GRADIENT_SHADOW_COLOR;
import static com.chindeo.skin.option.executor.ShadowLayoutExecutorBuilder.ATTRIBUTE_LAYOUT_BACKGROUND;
import static com.chindeo.skin.option.executor.ShadowLayoutExecutorBuilder.ATTRIBUTE_STROKE_COLOR;

import android.content.res.ColorStateList;
import android.graphics.drawable.ColorStateListDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

import com.chindeo.view.lihang.ShadowLayout;

import org.alee.component.skin.executor.SkinElement;
import org.alee.component.skin.executor.ViewSkinExecutor;
import org.jetbrains.annotations.NotNull;


@RestrictTo(RestrictTo.Scope.LIBRARY)
public class ShadowLayoutSkinExecutor<T extends ShadowLayout> extends ViewSkinExecutor<T> {

    public ShadowLayoutSkinExecutor(@NonNull @NotNull SkinElement fullElement) {
        super(fullElement);
    }


    @Override
    protected void applyColor(@NonNull @NotNull T view, int color, @NonNull @NotNull String attrName) {
        super.applyColor(view, color, attrName);
        switch (attrName){
            case ATTRIBUTE_LAYOUT_BACKGROUND:
                view.setLayoutBackground(color);
                break;
            case ATTRIBUTE_STROKE_COLOR:
                view.setStrokeColor(color);
                break;
            case ATTRIBUTE_GRADIENT_SHADOW_COLOR:
                view.setShadowColor(color);
                break;
        }
    }

    @Override
    protected void applyColor(@NonNull @NotNull T view, @NonNull @NotNull ColorStateList colorStateList, @NonNull @NotNull String attrName) {
        super.applyColor(view, colorStateList, attrName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            applyDrawable(view, new ColorStateListDrawable(colorStateList), attrName);
        } else {
            applyColor(view, colorStateList.getDefaultColor(), attrName);
        }
    }

    @Override
    protected void applyDrawable(@NonNull @NotNull T view, @NonNull @NotNull Drawable drawable, @NonNull @NotNull String attrName) {
        super.applyDrawable(view, drawable, attrName);
    }
}
