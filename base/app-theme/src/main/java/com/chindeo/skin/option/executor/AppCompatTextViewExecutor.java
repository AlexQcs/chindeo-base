package com.chindeo.skin.option.executor;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

import com.apkfuns.logutils.LogUtils;

import org.alee.component.skin.executor.SkinElement;
import org.alee.component.skin.executor.TextViewSkinExecutor;

public class AppCompatTextViewExecutor<T extends AppCompatTextView> extends TextViewSkinExecutor<T> {

    public AppCompatTextViewExecutor(@NonNull SkinElement fullElement) {
        super(fullElement);
    }

    @Override
    protected void applyColor(@NonNull T view, @NonNull ColorStateList colorStateList, @NonNull String attrName) {
        LogUtils.d("AppCompatTextViewExecutor applyColor -> " + attrName);
//        applyColor(view, colorStateList, attrName);
//        switch (attrName) {
//            case BaseExecutorBuilder.ATTRIBUTE_COLOR_TEXTCOLOR:
//                view.setTextColor(colorStateList);
//        }
    }

    @Override
    protected void applyColor(@NonNull T view, int color, @NonNull String attrName) {
//        applyColor(view, color, attrName);
//        switch (attrName) {
//            case BaseExecutorBuilder.ATTRIBUTE_COLOR_TEXTCOLOR:
//                view.setTextColor(color);
//                break;
//        }
    }

    @Override
    protected void applyDrawable(@NonNull T t, @NonNull Drawable drawable, @NonNull String attrName) {
        LogUtils.d("AppCompatTextViewExecutor applyDrawable");
        switch (attrName) {
            case BaseExecutorBuilder.ATTRIBUTE_DRAWABLE_END:
                t.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
                break;
            case BaseExecutorBuilder.ATTRIBUTE_DRAWABLE_START:
                t.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                break;
            case BaseExecutorBuilder.ATTRIBUTE_DRAWABLE_BOTTOM:
                t.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
                break;
            case BaseExecutorBuilder.ATTRIBUTE_DRAWABLE_TOP:
                t.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
                break;
        }
    }


}
