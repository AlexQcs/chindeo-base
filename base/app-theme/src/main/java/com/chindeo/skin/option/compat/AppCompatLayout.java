package com.chindeo.skin.option.compat;

import org.alee.component.skin.service.ThemeSkinService;

public final class AppCompatLayout {


    public static void init() {
        ThemeSkinService.getInstance().getCreateViewInterceptor().add(new AppCompatLayoutFactory());

    }
}
