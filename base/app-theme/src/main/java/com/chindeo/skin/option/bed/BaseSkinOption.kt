package com.chindeo.skin.option.bed

import android.content.Context
import android.util.Log
import com.apkfuns.logutils.LogUtils
import com.chindeo.repository.contants.FilePathConstants
import com.lazylibs.util.FileIOUtils
import org.alee.component.skin.service.IThemeSkinOption
import java.io.File
import java.util.LinkedHashSet

class BaseSkinOption(private val sourceName: String, private val context: Context) :
    IThemeSkinOption {
    override fun getStandardSkinPackPath(): LinkedHashSet<String> {
        val pathSet = LinkedHashSet<String>()
        Log.d("BaseSkinOption", "皮肤包名:" + sourceName)
        val path = FilePathConstants.getDir(FilePathConstants.APP_SKIN_DIR) + sourceName + ".skin"

        FileIOUtils.writeFileFromIS(
            path,
            context.getAssets().open("skin/$sourceName.skin")
        )
        pathSet.add(path)
        LogUtils.d("加载皮肤->$path")
        return pathSet
    }
}