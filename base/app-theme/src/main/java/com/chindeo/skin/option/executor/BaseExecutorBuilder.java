package com.chindeo.skin.option.executor;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import androidx.appcompat.widget.AppCompatTextView;

import com.chindeo.skin.R;

import org.alee.component.skin.executor.ISkinExecutor;
import org.alee.component.skin.executor.SkinElement;
import org.alee.component.skin.pack.ResourcesType;
import org.alee.component.skin.parser.IThemeSkinExecutorBuilder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BaseExecutorBuilder implements IThemeSkinExecutorBuilder {


    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_DRAWABLE_END = "drawableEnd";
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_DRAWABLE_START = "drawableStart";
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_DRAWABLE_TOP = "drawableTop";
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_DRAWABLE_BOTTOM = "drawableBottom";
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_COLOR_TEXTCOLOR = "textColor";

    private static final Map<Integer, String> SUPPORT_DRAWABLE_ATTR = new HashMap<>();
    private static final Map<Integer, String> SUPPORT_COLOR_ATTR = new HashMap<>();
    private static final Map<Integer, String> SUPPORT_ATTR = new HashMap<>();

    static {
        SUPPORT_DRAWABLE_ATTR.put(R.styleable.BaseAttr_android_drawableEnd, ATTRIBUTE_DRAWABLE_END);
        SUPPORT_DRAWABLE_ATTR.put(R.styleable.BaseAttr_android_drawableTop, ATTRIBUTE_DRAWABLE_TOP);
        SUPPORT_DRAWABLE_ATTR.put(R.styleable.BaseAttr_android_drawableBottom, ATTRIBUTE_DRAWABLE_BOTTOM);
        SUPPORT_DRAWABLE_ATTR.put(R.styleable.BaseAttr_android_drawableStart, ATTRIBUTE_DRAWABLE_START);
        SUPPORT_ATTR.putAll(SUPPORT_DRAWABLE_ATTR);
        SUPPORT_ATTR.putAll(SUPPORT_COLOR_ATTR);
    }

    @Override
    public Set<SkinElement> parse(@NonNull Context context, @NonNull AttributeSet attributeSet) {
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.BaseAttr);
        if (null == typedArray) {
            return null;
        }
        Set<SkinElement> elementSet = new HashSet<>();
        try {
            for (Integer key : SUPPORT_ATTR.keySet()) {
                try {
                    if (typedArray.hasValue(key)) {
                        SkinElement element = new SkinElement(SUPPORT_ATTR.get(key), typedArray.getResourceId(key, -1));
                        if (SUPPORT_DRAWABLE_ATTR.containsKey(key)) {
                            element.setResourcesType(ResourcesType.MIPMAP);
                        }
                        elementSet.add(element);
                    }
                } catch (Throwable ignored) {
                }
            }
        } catch (Throwable ignored) {
        } finally {
            typedArray.recycle();
        }
        return elementSet;
    }

    @Override
    public ISkinExecutor requireSkinExecutor(@NonNull View view, @NonNull SkinElement skinElement) {
        if (view instanceof AppCompatTextView) {
            return new AppCompatTextViewExecutor<>(skinElement);
        } else {
            return null;
        }
    }

    @Override
    public boolean isSupportAttr(@NonNull View view, @NonNull String s) {
        return SUPPORT_ATTR.containsValue(s);
    }
}
