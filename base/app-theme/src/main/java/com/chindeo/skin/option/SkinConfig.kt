package com.chindeo.skin.option

import androidx.annotation.DrawableRes
import org.alee.component.skin.service.IThemeSkinOption

data class SkinConfig(val option: IThemeSkinOption, val name: String, val skinId: Int) {

    @DrawableRes
    var coverRes: Int? = null


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SkinConfig

        if (skinId != other.skinId) return false

        return true
    }

    override fun hashCode(): Int {
        return skinId
    }


}