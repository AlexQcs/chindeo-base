package com.chindeo.skin.option.executor;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;

import com.chindeo.skin.R;
import com.chindeo.view.lihang.ShadowLayout;

import org.alee.component.skin.executor.ISkinExecutor;
import org.alee.component.skin.executor.SkinElement;
import org.alee.component.skin.executor.ViewSkinExecutor;
import org.alee.component.skin.factory2.IExpandedFactory2;
import org.alee.component.skin.parser.IThemeSkinExecutorBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ShadowLayoutExecutorBuilder implements IThemeSkinExecutorBuilder {

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_LAYOUT_BACKGROUND = "hl_layoutBackground";

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_STROKE_COLOR = "hl_strokeColor";

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_GRADIENT_START_COLOR = "hl_startColor";

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_GRADIENT_CENTER_COLOR = "hl_centerColor";

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_GRADIENT_END_COLOR = "hl_endColor";

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public static final String ATTRIBUTE_GRADIENT_SHADOW_COLOR = "hl_shadowColor";

    private static final Map<Integer, String> SUPPORT_ATTR = new HashMap<>();

    static {
        SUPPORT_ATTR.put(R.styleable.ShadowLayout_hl_layoutBackground, ATTRIBUTE_LAYOUT_BACKGROUND);
        SUPPORT_ATTR.put(R.styleable.ShadowLayout_hl_strokeColor, ATTRIBUTE_STROKE_COLOR);
        SUPPORT_ATTR.put(R.styleable.ShadowLayout_hl_startColor, ATTRIBUTE_GRADIENT_START_COLOR);
        SUPPORT_ATTR.put(R.styleable.ShadowLayout_hl_centerColor, ATTRIBUTE_GRADIENT_CENTER_COLOR);
        SUPPORT_ATTR.put(R.styleable.ShadowLayout_hl_endColor, ATTRIBUTE_GRADIENT_END_COLOR);
        SUPPORT_ATTR.put(R.styleable.ShadowLayout_hl_shadowColor, ATTRIBUTE_GRADIENT_SHADOW_COLOR);
    }

    @Override
    public Set<SkinElement> parse(@NonNull @NotNull Context context, @NonNull @NotNull AttributeSet attributeSet) {
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.BasicSupportAttr);
        if (null == typedArray) {
            return null;
        }
        Set<SkinElement> elementSet = new HashSet<>();
        try {
            for (Integer key : SUPPORT_ATTR.keySet()) {
                try {
                    if (typedArray.hasValue(key)) {
                        elementSet.add(new SkinElement(SUPPORT_ATTR.get(key), typedArray.getResourceId(key, -1)));
                    }
                } catch (Throwable ignored) {
                }
            }
        } catch (Throwable ignored) {
        } finally {
            typedArray.recycle();
        }
        return elementSet;
    }

    @Override
    public ISkinExecutor requireSkinExecutor(@NonNull @NotNull View view, @NonNull @NotNull SkinElement skinElement) {
        if (view instanceof ShadowLayout) {
            return new ShadowLayoutSkinExecutor(skinElement);
        } else {
            return new ViewSkinExecutor<>(skinElement);
        }
    }

    @Override
    public boolean isSupportAttr(@NonNull @NotNull View view, @NonNull @NotNull String attrName) {
        return SUPPORT_ATTR.containsValue(attrName);
    }
}
