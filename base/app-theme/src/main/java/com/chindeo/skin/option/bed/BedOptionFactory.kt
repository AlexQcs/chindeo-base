package com.chindeo.skin.option.bed


import android.content.Context
import org.alee.component.skin.service.IOptionFactory
import org.alee.component.skin.service.IThemeSkinOption

class BedOptionFactory(val context: Context) : IOptionFactory {

    override fun defaultTheme(): Int {
        return 0
    }

    override fun requireOption(skinId: Int): IThemeSkinOption? {
        return BedSkinConstants.getSkinOption(BedSkinConstants.Beds, skinId,context)
    }

}