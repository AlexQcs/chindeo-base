package com.chindeo.skin.option.compat;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import org.alee.component.skin.factory2.IExpandedFactory2;
import org.alee.component.skin.service.Config;
import org.alee.component.skin.util.PrintUtil;

public class AppCompatLayoutFactory implements IExpandedFactory2 {

    /**
     * {@link ConstraintLayout} 类名
     */
    private final static String CLASS_NAME = AppCompatTextView.class.getName();
    /**
     * 是否使用换肤功能
     */
    private static final String ATTRIBUTE_SKIN_ENABLE = "enable";
    /**
     * 自定义属性命名空间
     */
    private static final String ATTRIBUTE_NAMESPACE = "http://schemas.android.com/android/skin";

    @NonNull
    @Override
    public View onCreateView(@Nullable View view, @Nullable View view1, @NonNull String name, @NonNull Context context, @NonNull AttributeSet attributeSet) {
        try {
            if (attributeSet.getAttributeBooleanValue(ATTRIBUTE_NAMESPACE, ATTRIBUTE_SKIN_ENABLE, Config.SkinMode.REPLACE_MARKED != Config.getInstance().getSkinMode())) {
                return TextUtils.equals(CLASS_NAME, name) ? new AppCompatTextView(context, attributeSet) : view;
            }
        } catch (Throwable e) {
            PrintUtil.getInstance().printE(e);
        }
        return view;
    }
}
