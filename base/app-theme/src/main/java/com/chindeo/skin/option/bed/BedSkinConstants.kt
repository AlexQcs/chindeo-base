package com.chindeo.skin.option.bed

import android.content.Context
import com.chindeo.skin.R
import com.chindeo.skin.option.SkinConfig
import org.alee.component.skin.service.IThemeSkinOption

object BedSkinConstants {


    interface SkinInterface {
        fun configs(context: Context): MutableList<SkinConfig>
        fun config(skinId: Int, context: Context): IThemeSkinOption?
    }

    object Beds : SkinInterface {

        enum class SkinId(val id: Int) {
            GREEN(1),
            BLUE(2),
            ORANGE(3),
            PINK(4),
            PURPLE(5),
            ;
        }
        //前端需要传颜色的英文名
        override fun configs(context: Context): MutableList<SkinConfig> = mutableListOf(
            SkinConfig(BaseSkinOption("bed_skin_green", context), "green", SkinId.GREEN.id).apply {
                coverRes = R.drawable.bed_sick_skin_gradient_raduis_cyan
            },
            SkinConfig(BaseSkinOption("bed_skin_blue", context), "blue", SkinId.BLUE.id).apply {
                coverRes = R.drawable.bed_sick_skin_gradient_raduis_blue
            },
            SkinConfig(BaseSkinOption("bed_skin_orange", context), "orange", SkinId.ORANGE.id).apply {
                coverRes = R.drawable.bed_sick_skin_gradient_raduis_orange
            },
            SkinConfig(BaseSkinOption("bed_skin_pink", context), "pink", SkinId.PINK.id).apply {
                coverRes = R.drawable.bed_sick_skin_gradient_raduis_pink
            },
            SkinConfig(BaseSkinOption("bed_skin_purple", context), "purple", SkinId.PURPLE.id).apply {
                coverRes = R.drawable.bed_sick_skin_gradient_raduis_purple
            }
        )

        override fun config(skinId: Int, context: Context): IThemeSkinOption? {

            for (item in configs(context)) {
                if (item.skinId == skinId) {
                    return item.option
                }
            }
            return null
        }
    }

    fun getSkinOption(imp: SkinInterface, skinId: Int, context: Context): IThemeSkinOption? {
        return imp.config(skinId, context)
    }

}