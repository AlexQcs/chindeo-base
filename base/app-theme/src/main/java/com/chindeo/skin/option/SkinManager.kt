package com.chindeo.skin.option

import android.app.Application
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import org.alee.component.skin.service.ISwitchThemeSkinObserver
import org.alee.component.skin.service.ThemeSkinService

class SkinManager {

    companion object {

        fun init(context: Application) {

//            WindowManager.getInstance().init(context, NursesOptionFactory(context))
//            ConstraintLayoutCompat.init()
//            AppCompatLayout.init()
//            ThemeSkinService.getInstance().addThemeSkinExecutorBuilder(ShadowLayoutExecutorBuilder())
//            ThemeSkinService.getInstance().addThemeSkinExecutorBuilder(BaseExecutorBuilder())
//
//            Config.getInstance().skinMode = Config.SkinMode.REPLACE_ALL
//            Config.getInstance().isEnableDebugMode = true
//            Config.getInstance().isEnableStrictMode = false
//            Config.getInstance().performanceMode = Config.PerformanceMode.PERFORMANCE_PRIORITY
//
//            try {
//                val skinPacks: Array<String> = context.assets.list("skin")!!
//                Observable.fromIterable(skinPacks.toMutableList()).subscribeOn(Schedulers.io())
//                        .doOnNext { skinName ->
//                            LogUtils.d("皮肤资源 assets中获取->${skinName}")
//                            val destFile =
//                                    File(FilePathConstants.getDir(FilePathConstants.APP_SKIN_DIR) + skinName)
//                            //                            if (!destFile.exists()){
//                            FileIOUtils.writeFileFromIS(
//                                    destFile,
//                                    context.getAssets().open("skin/$skinName")
//                            )
//                        }
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .doOnError { throwable ->
//                            throwable.printStackTrace()
//                            LogUtils.d("皮肤资源 copy皮肤失败" + throwable.message)
//                        }
//                        .doOnComplete {
//                            LogUtils.d("皮肤资源 皮肤加载完成")
//                            val cacheSkinId = AppSettingCache.getCurrentSkin()
//                            if (cacheSkinId != -1) {
//                                ThemeSkinService.getInstance().switchThemeSkin(cacheSkinId);
//                            }
//                        }
//                        .subscribe()
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }
//
//
        }
    }

}


fun Context?.getCurrentSkinColor(id: Int): Int {
    this?.let {
        return ContextCompat.getColor(it,id)
    }
    return id
}

fun Context.getCurrentSkinDrawable(id: Int): Drawable? {
    return getDrawable(id)
}



fun View.skin(attr: Attr, res: Int) {


    if (TextUtils.equals(attr.name, Attr.src.name)){
        Glide.with(this).load(res).into(this as ImageView)
    }else  if (TextUtils.equals(attr.name, Attr.background.name)){
        this.setBackgroundResource(res)
    }else  if (TextUtils.equals(attr.name, Attr.textColor.name)){
        val view = this as TextView
        view.setTextColor(res)
    }else  if (TextUtils.equals(attr.name, Attr.drawableStart.name)){
        val drawable = resources.getDrawable(res)
        drawable?.let {
            it.setBounds(0, 0, it.minimumWidth, it.minimumHeight)
            val view = this as TextView
            view.setCompoundDrawables(it,null,null,null)
        }

    }else  if (TextUtils.equals(attr.name, Attr.drawableEnd.name)){
        val drawable = resources.getDrawable(res)
        drawable?.let {
            it.setBounds(0, 0, it.minimumWidth, it.minimumHeight)
            val view = this as TextView
            view.setCompoundDrawables(null,null,it,null)
        }
    }

//    WindowManager.getInstance().getWindowProxy(context)?.addEnabledThemeSkinView(this, SkinElement(attr.name, res))
}

fun AppCompatTextView.textColor(res: Int) {
    setTextColor(res)
//    WindowManager.getInstance().getWindowProxy(context)?.addEnabledThemeSkinView(this, SkinElement(Attr.textColor.name, res).apply {
//        resourcesType = ResourcesType.COLOR
//    })
}

fun getAnyColorStateList(){
//    ThemeSkinService.getInstance().currentThemeSkinPack.getColorStateList()
}

fun getAnyColorStateList(id: Int): ColorStateList? {
    return ThemeSkinService.getInstance().currentThemeSkinPack.getColorStateList(id)
}

fun subscribeAnySwitchThemeSkin(observer: ISwitchThemeSkinObserver) {
    ThemeSkinService.getInstance().subscribeSwitchThemeSkin(observer)
}

fun switchAnyThemeSkin(themeId: Int) {
//    ThemeSkinService.getInstance().switchThemeSkin(themeId)
}

enum class Attr {
    background,
    src,
    drawableEnd, drawableStart, drawableTop, drawableBottom,
    textColor,
    progressDrawable
}