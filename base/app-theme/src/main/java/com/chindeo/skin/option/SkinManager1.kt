//package com.chindeo.skin.option
//
//import android.app.Application
//import android.content.Context
//import android.content.res.ColorStateList
//import android.graphics.drawable.Drawable
//import android.view.View
//import androidx.appcompat.widget.AppCompatTextView
//import com.apkfuns.logutils.LogUtils
//import com.chindeo.repository.contants.FilePathConstants
//import com.chindeo.repository.mmkv.impl.AppSettingCache
//import com.chindeo.skin.option.compat.AppCompatLayout
//import com.chindeo.skin.option.executor.BaseExecutorBuilder
//import com.chindeo.skin.option.executor.ShadowLayoutExecutorBuilder
//import com.chindeo.skin.option.nurses.NursesOptionFactory
//import com.lazylibs.util.FileIOUtils
//import io.reactivex.Observable
//import io.reactivex.android.schedulers.AndroidSchedulers
//import io.reactivex.schedulers.Schedulers
//import org.alee.component.skin.compat.ConstraintLayoutCompat
//import org.alee.component.skin.executor.SkinElement
//import org.alee.component.skin.pack.ResourcesType
//import org.alee.component.skin.page.WindowManager
//import org.alee.component.skin.service.Config
//import org.alee.component.skin.service.ISwitchThemeSkinObserver
//import org.alee.component.skin.service.ThemeSkinService
//import java.io.File
//import java.io.IOException
//
//class SkinManager {
//
//    companion object {
//
//        fun init(context: Application) {
//
//            WindowManager.getInstance().init(context, NursesOptionFactory(context))
//            ConstraintLayoutCompat.init()
//            AppCompatLayout.init()
//            ThemeSkinService.getInstance().addThemeSkinExecutorBuilder(ShadowLayoutExecutorBuilder())
//            ThemeSkinService.getInstance().addThemeSkinExecutorBuilder(BaseExecutorBuilder())
//
//            Config.getInstance().skinMode = Config.SkinMode.REPLACE_ALL
//            Config.getInstance().isEnableDebugMode = true
//            Config.getInstance().isEnableStrictMode = false
//            Config.getInstance().performanceMode = Config.PerformanceMode.PERFORMANCE_PRIORITY
//
//            try {
//                val skinPacks: Array<String> = context.assets.list("skin")!!
//                Observable.fromIterable(skinPacks.toMutableList()).subscribeOn(Schedulers.io())
//                    .doOnNext { skinName ->
//                        LogUtils.d("皮肤资源 assets中获取->${skinName}")
//                        val destFile =
//                            File(FilePathConstants.getDir(FilePathConstants.APP_SKIN_DIR) + skinName)
//                        //                            if (!destFile.exists()){
//                        FileIOUtils.writeFileFromIS(
//                            destFile,
//                            context.getAssets().open("skin/$skinName")
//                        )
//                    }
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .doOnError { throwable ->
//                        throwable.printStackTrace()
//                        LogUtils.d("皮肤资源 copy皮肤失败" + throwable.message)
//                    }
//                    .doOnComplete {
//                        LogUtils.d("皮肤资源 皮肤加载完成")
//                        val cacheSkinId = AppSettingCache.getCurrentSkin()
//                        if (cacheSkinId != -1) {
//                            ThemeSkinService.getInstance().switchThemeSkin(cacheSkinId);
//                        }
//                    }
//                    .subscribe()
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }
//
//
//        }
//    }
//
//}
//
//fun Context.getCurrentSkinColor(id: Int): Int {
//    return ThemeSkinService.getInstance().currentThemeSkinPack.getColor(id)
//}
//
//fun Context.getCurrentSkinDrawable(id: Int): Drawable? {
//    return ThemeSkinService.getInstance().currentThemeSkinPack.getDrawable(id)
//}
//
//
//fun View.skin(attr: Attr, res: Int) {
//    WindowManager.getInstance().getWindowProxy(context)?.addEnabledThemeSkinView(this, SkinElement(attr.name, res))
//}
//
//fun AppCompatTextView.textColor(res: Int) {
//    WindowManager.getInstance().getWindowProxy(context)?.addEnabledThemeSkinView(this, SkinElement(Attr.textColor.name, res).apply {
//        resourcesType = ResourcesType.COLOR
//    })
//}
//
//fun getAnyColorStateList(){
//    ThemeSkinService.getInstance().currentThemeSkinPack.getColorStateList()
//}
//
//fun getAnyColorStateList(id: Int): ColorStateList? {
//    return ThemeSkinService.getInstance().currentThemeSkinPack.getColorStateList(id)
//}
//
//fun subscribeAnySwitchThemeSkin(observer: ISwitchThemeSkinObserver) {
//    ThemeSkinService.getInstance().subscribeSwitchThemeSkin(observer)
//}
//
//fun switchAnyThemeSkin(themeId: Int) {
//    ThemeSkinService.getInstance().switchThemeSkin(themeId)
//}
//
//enum class Attr {
//    background,
//    src,
//    drawableEnd, drawableStart, drawableTop, drawableBottom,
//    textColor,
//}