package com.chindeo.skin.option.bed

import android.app.Application
import android.view.View
import com.chindeo.skin.option.Attr
import org.alee.component.skin.compat.ConstraintLayoutCompat
import org.alee.component.skin.executor.SkinElement
import org.alee.component.skin.page.WindowManager
import org.alee.component.skin.service.Config
import org.alee.component.skin.service.ThemeSkinService

/**
 * 床旁换肤管理
 * Created by xiemaohui on 2022/11/16
 */
class BedSkinManager {

    companion object {

        fun init(context: Application) {

            Config.getInstance().setSkinMode(Config.SkinMode.REPLACE_ALL)
            WindowManager.getInstance().init(context, BedOptionFactory(context))
            ConstraintLayoutCompat.init()

            Config.getInstance().isEnableDebugMode = true
            Config.getInstance().isEnableStrictMode = false
            Config.getInstance().performanceMode = Config.PerformanceMode.PERFORMANCE_PRIORITY
        }
    }

}

//fun Context.getCurrentSkinColor(id: Int): Int {
//    return ThemeSkinService.getInstance().currentThemeSkinPack.getColor(id)
//}
//
//fun Context.getCurrentSkinDrawable(id: Int): Drawable? {
//    return ThemeSkinService.getInstance().currentThemeSkinPack.getDrawable(id)
//}
//
//
fun View.dynamicSkin(attr: Attr, res: Int) {
    WindowManager.getInstance().getWindowProxy(context)
        ?.addEnabledThemeSkinView(this, SkinElement(attr.name, res))
}

//
//fun AppCompatTextView.textColor(res: Int) {
//    WindowManager.getInstance().getWindowProxy(context)?.addEnabledThemeSkinView(this, SkinElement(Attr.textColor.name, res).apply {
//        resourcesType = ResourcesType.COLOR
//    })
//}
//
//fun getAnyColorStateList(){
//    ThemeSkinService.getInstance().currentThemeSkinPack.getColorStateList()
//}
//
//fun getAnyColorStateList(id: Int): ColorStateList? {
//    return ThemeSkinService.getInstance().currentThemeSkinPack.getColorStateList(id)
//}
//
//fun subscribeAnySwitchThemeSkin(observer: ISwitchThemeSkinObserver) {
//    ThemeSkinService.getInstance().subscribeSwitchThemeSkin(observer)
//}
//
fun switchAnyThemeSkin(themeId: Int) {
    ThemeSkinService.getInstance().switchThemeSkin(themeId)
}
//
//enum class Attr {
//    background,
//    src,
//    drawableEnd, drawableStart, drawableTop, drawableBottom,
//    textColor,
//
