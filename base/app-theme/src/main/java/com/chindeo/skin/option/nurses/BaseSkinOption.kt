package com.chindeo.skin.option.nurses

import com.apkfuns.logutils.LogUtils
import com.chindeo.repository.contants.FilePathConstants
import org.alee.component.skin.service.IThemeSkinOption
import java.util.LinkedHashSet

class BaseSkinOption(private val sourceName: String) : IThemeSkinOption {

    override fun getStandardSkinPackPath(): LinkedHashSet<String> {
        val pathSet = LinkedHashSet<String>()
        val path = FilePathConstants.getDir(FilePathConstants.APP_SKIN_DIR) + sourceName + ".skin"
        pathSet.add(path)
        LogUtils.d("加载皮肤->$path")
        return pathSet
    }

}