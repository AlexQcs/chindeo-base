package com.chindeo.skin.option.executor

import android.content.Context
import android.util.AttributeSet
import android.view.View
import org.alee.component.skin.factory2.IExpandedFactory2
import com.chindeo.view.lihang.ShadowLayout

/**
 * 自定义View孵化工厂
 * Created by xiemaohui on 2022/11/21
 */
class ShadowViewFactor : IExpandedFactory2 {

    companion object {
        /**
         * [CLASS_NAME] 类名
         */
        private val CLASS_NAME = ShadowLayout::class.java.name
    }

    /**
     * 创建View
     *
     * @param originalView 上一个IExpandedFactory生成的View
     * @param parent       父View
     * @param name         名称
     * @param context      [Context]
     * @param attrs        [AttributeSet]
     * @return 生成的View
     */
    override fun onCreateView(
        originalView: View?,
        parent: View?,
        name: String,
        context: Context,
        attrs: AttributeSet
    ): View {
        return if (CLASS_NAME == name) {
            ShadowLayout(context, attrs)
        } else {
            originalView!!
        }
    }


}