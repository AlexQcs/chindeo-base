package com.chindeo.skin.option

import com.chindeo.skin.R
import com.chindeo.skin.option.nurses.BaseSkinOption
import org.alee.component.skin.service.IThemeSkinOption

object SkinConstants {


    interface SkinInterface {
        fun configs(): MutableList<SkinConfig>
        fun config(skinId: Int): IThemeSkinOption?
    }

    object Nurses : SkinInterface {

        enum class SkinId(val id: Int) {
            BLUE(1),
            GREEN(2),
            ORANGE(3),
            PINK(4),
            PURPLE(5),
            ;
        }

        override fun configs(): MutableList<SkinConfig> = mutableListOf(
                SkinConfig(BaseSkinOption("nurses_skin_blue"), "去海边", SkinId.BLUE.id).apply {
                    coverRes = R.drawable.nurses_bg_skin_blue
                },
                SkinConfig(BaseSkinOption("nurses_skin_orange"), "喵星人", SkinId.ORANGE.id).apply {
                    coverRes = R.drawable.nurses_bg_skin_orange
                },
                SkinConfig(BaseSkinOption("nurses_skin_pink"), "比心", SkinId.PINK.id).apply {
                    coverRes = R.drawable.nurses_bg_skin_pink
                },
                SkinConfig(BaseSkinOption("nurses_skin_green"), "SUMMER", SkinId.GREEN.id).apply {
                    coverRes = R.drawable.nurses_bg_skin_green
                },
                SkinConfig(BaseSkinOption("nurses_skin_purple"), "心语", SkinId.PURPLE.id).apply {
                    coverRes = R.drawable.nurses_bg_skin_purple
                }
        )

        override fun config(skinId: Int): IThemeSkinOption? {
            for (item in configs()) {
                if (item.skinId == skinId) {
                    return item.option
                }
            }
            return null
        }
    }

    fun getSkinOption(imp: SkinInterface, skinId: Int): IThemeSkinOption? {
        return imp.config(skinId)
    }

}