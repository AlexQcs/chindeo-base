package com.chindeo.skin.option.nurses

import android.content.Context
import com.chindeo.skin.option.SkinConstants
import org.alee.component.skin.service.IOptionFactory
import org.alee.component.skin.service.IThemeSkinOption

class NursesOptionFactory(val context: Context) : IOptionFactory {

    override fun defaultTheme(): Int {
        return SkinConstants.Nurses.SkinId.BLUE.id
    }

    override fun requireOption(skinId: Int): IThemeSkinOption? {
        return SkinConstants.getSkinOption(SkinConstants.Nurses, skinId)
    }

}