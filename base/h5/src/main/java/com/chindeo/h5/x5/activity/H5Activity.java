package com.chindeo.h5.x5.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.chindeo.h5.R;
import com.chindeo.h5.x5.H5;
import com.chindeo.h5.x5.H5PrivateUtil;
import com.chindeo.h5.x5.fragment.H5Fragment;
import com.chindeo.h5.x5.view.KeyBoardListener;
import com.lazylibs.component.ui.activity.ToolbarContainerActivity;
import com.lazylibs.lifecycle.ActivityManager;


public class H5Activity extends ToolbarContainerActivity {
    private H5Fragment h5Fragment;

    @Override
    public void setContentView(int layoutResID) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.setContentView(layoutResID);
    }

    @NonNull
    @Override
    protected String getToolbarTitle() {
        if (showToolbar) {
            String title = getIntent().getStringExtra(H5.TITLE);
            if (TextUtils.isEmpty(title)) {
                if (getIntent().hasExtra(H5.TITLE_ICON)) {
                    toolbarTitle.setVisibility(View.VISIBLE);
                    toolbarTitle.setCompoundDrawablesWithIntrinsicBounds(
                            getIntent().getIntExtra(H5.TITLE_ICON, 0), 0, 0, 0);
                }
                return "";
            } else {
                return title;
            }
        }
        return "";
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    boolean showToolbar = true;

    @Override
    public void init(Bundle savedInstanceState) {
        KeyBoardListener.getInstance(this).init();
        showToolbar = !(
                TextUtils.isEmpty(getIntent().getStringExtra(H5.TITLE)) ||
                        (
                                getIntent().hasExtra(H5.NEED_HEADER) &&
                                        "1".equals(getIntent().getStringExtra(H5.NEED_HEADER))
                        )
        );
        super.init(savedInstanceState);
        H5PrivateUtil.dartStatusBar(getWindow());
        View rootView = findViewById(android.R.id.content);
        if (rootView != null) {
            rootView.setBackgroundColor(Color.WHITE);
        }
        if (getSupportActionBar() != null && !showToolbar) {
            getSupportActionBar().hide();
        } else {
            ((ConstraintLayout.LayoutParams) toolbar.getLayoutParams()).topMargin = H5PrivateUtil.getStatusBarHeight(this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            getWindow().getDecorView().setOnApplyWindowInsetsListener(
                    (v, insets) -> {
                        int tempHeight = H5PrivateUtil.getNavigationBarHeight(H5Activity.this);
                        boolean isShowing = false;
                        int b;
                        if (insets != null) {
                            b = insets.getSystemWindowInsetBottom();
                            isShowing = b == tempHeight;
                        }
                        int navigationBarHeight = isShowing ? tempHeight : 0;
                        findViewById(R.id.container).setPadding(0, 0, 0, navigationBarHeight);
                        return insets;
                    });
        }
    }

    @NonNull
    @Override
    public Fragment newFragmentInstance() {
        return h5Fragment = H5Fragment.newInstance(getIntent() != null && getIntent().getExtras() != null ? getIntent().getExtras() : new Bundle());
    }

    public static void start(Context context, String url, Bundle extras) {
        start(context, url, extras, -1);
    }

    public static void start(Context context, String url, Bundle extras, int requestCode) {
        if (TextUtils.isEmpty(url)) return;
        Intent intent = new Intent(context, H5Activity.class);
        intent.putExtra(H5.URL, url);
        intent.putExtras(extras);
        if (requestCode != -1) {
            ActivityManager.getInstance().topOfStackActivity().startActivityForResult(intent, requestCode);
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent, extras);
        }
    }

    public static void start(Fragment fragment, String url, Bundle extras, int requestCode) {
        if (TextUtils.isEmpty(url)) return;
        if (fragment == null || fragment.getContext() == null) {
            return;
        }
        Intent intent = new Intent(fragment.getContext(), H5Activity.class);
        intent.putExtra(H5.URL, url);
        intent.putExtras(extras);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (requestCode != -1) {
            fragment.startActivityForResult(intent, requestCode, extras);
        } else {
            fragment.startActivity(intent, extras);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (h5Fragment != null) {
            h5Fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
