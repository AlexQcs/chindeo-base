package com.chindeo.h5.x5.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class UnTouch extends X5WebView {

    public UnTouch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnTouch(Context context) {
        super(context);
    }

    //禁止滑动
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}