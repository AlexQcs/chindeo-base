package com.chindeo.h5.x5.web

import android.app.Activity
import android.webkit.JavascriptInterface
import android.webkit.WebView
import org.json.JSONException
import org.json.JSONObject

/** 和js交互的时候 需要用到这个类 注意加上注解 不然js收不到值   @JavascriptInterface
 * Created by zqs on 2022/5/11
 */
//class TemplateJavaInterface(val activity: Activity, val webView: WebView) {
//
//    var onJavascriptInterfaceListener:OnJavascriptInterfaceListener? =null
//
//    fun setOnJavascriptIListener(onJavascriptInterfaceListener: OnJavascriptInterfaceListener?) {
//        this.onJavascriptInterfaceListener = onJavascriptInterfaceListener
//    }
//
//    @JavascriptInterface
//    fun onAndroidFunction(json: String?): String? {
//        var returnString: String? = "success"
//
//        // json { function: clickVoice,param{} }
//        if (onJavascriptInterfaceListener != null) {
//            try {
//                val jsonObject = JSONObject(json)
//                val function = jsonObject.getString("function")
//                returnString = function
//                if ("startVoice" == function) {
//                    onJavascriptInterfaceListener!!.onStartVoice()
//                } else if ("stopVoice" == function) {
//                    onJavascriptInterfaceListener!!.onStopVoice()
//                } else if ("voiceAnalysis" == function) {
//                    val paramJson = jsonObject.getJSONObject("param")
//                    onJavascriptInterfaceListener!!.onVoiceAnalysis(paramJson.toString())
//                } else if ("clickVoice" == function) {
//                    onJavascriptInterfaceListener!!.onClickVoice()
//                }
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            }
//        }
//        return returnString
//    }
//
//}