package com.chindeo.h5.x5.web

/**
 *
 * Created by xiemaohui on 2022/10/8
 */
interface OnJavascriptInterfaceListener {
    fun onStartVoice()

    fun onStopVoice()

    fun onVoiceAnalysis(analysisJson: String?)

    fun onClickVoice()
}