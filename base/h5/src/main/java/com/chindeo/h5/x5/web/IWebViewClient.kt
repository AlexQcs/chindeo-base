package com.chindeo.h5.x5.web

import android.annotation.TargetApi
import android.graphics.Bitmap
import android.os.Build
import android.webkit.*
import androidx.annotation.RequiresApi
import com.apkfuns.logutils.LogUtils
import com.chindeo.repository.RepositoryComponent
import com.lazylibs.util.ToastUtils

/**
 * Created by zqs on 2022/2/16
 */
class IWebViewClient(val webView: IWebView): WebViewClient() {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest): Boolean {
        LogUtils.d( "拦截url地址: " + request.url.toString())
        return webView!!.shouldOverrideUrlLoading(request.url.toString())
    }

    override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
        LogUtils.d( "拦截url地址: $url")
        return webView!!.shouldOverrideUrlLoading(url)
    }

    override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
        LogUtils.d("开始请求的url地址: $url")
        /*val token = SPUtils.decodeString(RoutePath.MmKv.LOGIN_TOKEN)
        //传值 给js
        if (!TextUtils.isEmpty(token)) {
            val js = "window.localStorage.setItem('Admin-Token','$token');"
            Log.d("-------", token)
            view.evaluateJavascript(js) { value: String? ->
                Log.d("-------", value!!)
            }
        }*/
        super.onPageStarted(view, url, favicon)
    }

    override fun onPageFinished(view: WebView, url: String) {
        super.onPageFinished(view, url)
        LogUtils.d("结束请求的url地址: $url")
        /*val token = SPUtils.decodeString(RoutePath.MmKv.LOGIN_TOKEN)
        Log.d("-------", token)
        if (!TextUtils.isEmpty(token)) {
            val js = "window.localStorage.setItem('Admin-Token','$token');"
            view.evaluateJavascript(js) { value: String? ->
                Log.d("-------", value!!)
            }
        }*/
    }

    @TargetApi(android.os.Build.VERSION_CODES.M)
    override fun onReceivedHttpError(
        view: WebView?,
        request: WebResourceRequest?,
        errorResponse: WebResourceResponse?
    ) {
        //404与500处理
        super.onReceivedHttpError(view, request, errorResponse)
        val statusCode = errorResponse!!.statusCode
        if (404 == statusCode || 500 == statusCode) {
//            view?.loadUrl("about:blank") // 避免出现默认的错误界面
            LogUtils.e("url: ${request?.url} 错误：${errorResponse.reasonPhrase} $statusCode")
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onReceivedError(
        view: WebView?,
        request: WebResourceRequest?,
        error: WebResourceError?
    ) {
        super.onReceivedError(view, request, error)
        // 断网或者网络连接超时
        if (error?.errorCode == ERROR_HOST_LOOKUP || error?.errorCode == ERROR_CONNECT || error?.errorCode == ERROR_TIMEOUT) {
            view?.loadUrl("about:blank"); // 避免出现默认的错误界面
            LogUtils.e("url: ${request?.url} 错误：${error.description} ${error.errorCode}")
        }
    }
}