package com.chindeo.h5.x5;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.alibaba.fastjson.JSON;
import com.chindeo.h5.x5.activity.H5Activity;
import com.chindeo.repository.RepositoryComponent;
import com.chindeo.repository.mmkv.impl.UrlCache;
import com.chindeo.repository.util.AppConfigManager;
import com.lazylibs.util.ToastUtils;
import java.util.HashMap;

public interface H5 {

    static String getBaseUrl() {
        return UrlCache.getUrlConfig().getResUrl();
    }


    String URL = "H5.url";
    String TITLE = "H5.title";
    String NEED_BACK = "H5.need.back";
    String USER_AGENT = "H5.user.agent";
    String TITLE_ICON = "H5.title.icon";
    String NEED_HEADER = "H5.need.header";
    String NEED_SCROLL = "H5.need.scroll";
    String HEADER_STRING = "H5.header.string";
    String TOKEN_HANDLER = "H5.login.token.handler";
    String JAVA_SCRIPT_INTERFACE = "H5.javascript.interface";


    static void openH5(Context context, String title, String url) {
        Bundle extras = new Bundle();
        extras.putString(H5.URL, url);
        extras.putString(H5.TITLE, title);
        extras.putString(H5.USER_AGENT, AppConfigManager.get().getUserAgent());
        extras.putString(H5.NEED_HEADER, TextUtils.isEmpty(title) ? "1" : "0");
        extras.putString(H5.NEED_BACK, TextUtils.isEmpty(title) ? "1" : "0");
        openH5(context, extras);
    }

    static void openH5(Context context, Bundle extras) {
        if (extras == null) return;
        H5Activity.start(context, extras.getString(H5.URL), extras);
    }


    static Bundle getBaseExtras(String title, String url) {
        Bundle extras = new Bundle();
        extras.putString(TITLE, title);
        extras.putString(URL, url);
        extras.putString(NEED_HEADER, "0");
        extras.putString(NEED_BACK, "0");
        return extras;
    }

    static Bundle getBaseExtras(String title, String url, boolean needScroll) {
        Bundle extras = new Bundle();
        extras.putString(TITLE, title);
        extras.putString(URL, url);
        extras.putString(NEED_HEADER, "0");
        extras.putString(NEED_BACK, "0");
        extras.putBoolean(NEED_SCROLL, needScroll);
        return extras;
    }

    static Bundle getBaseExtras(int titleIcon, String url) {
        Bundle extras = new Bundle();
        extras.putInt(TITLE_ICON, titleIcon);
        extras.putString(URL, url);
        extras.putString(NEED_HEADER, "0");
        extras.putString(NEED_BACK, "0");
        return extras;
    }

    static Bundle getBaseExtras(String title, String url, String needHeader, String needBack) {
        Bundle extras = new Bundle();
        extras.putString(TITLE, title);
        extras.putString(URL, url);
        extras.putString(NEED_HEADER, needHeader);
        extras.putString(NEED_BACK, needBack);
        return extras;
    }

    static void openBrowser(Context context, String uri) {
        Intent intent = new Intent();
        intent.setData(Uri.parse(uri));
        intent.setAction(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            // 跳转原生
            context.startActivity(intent);
        } catch (Exception e) {
            // 原生不存在或者其他错误
            e.printStackTrace();
            try {
                intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                context.startActivity(intent);
            } catch (Exception e1) {
                e.printStackTrace();
            }
        }
    }

    //    fromapp                             app名称
//    token                                  登陆token，有就传没有就不传
//    appversion                        当前版本
//    needHeader                       1需要h5头部, 0 不需要h5头部
//    needBack                           1 需要返回按钮,0 不需要返回按钮（默认为需要）
    static String getHeaderString(String loginToken, String needHeader, String needBack) {
        return JSON.toJSONString(new HashMap<String, String>() {{
            if (!TextUtils.isEmpty(loginToken)) {
                put("token", loginToken);
            } else {
                put("token", "");
            }
        }});
    }


    static boolean requireParams(Params params) {
        if (params == null) {
            Log.d("H5", "缺少必要的参数params");
            return false;
//            throw new IllegalArgumentException("缺少必要的参数params");
        }
        if (TextUtils.isEmpty(params.url)) {
            Log.d("H5", "缺少必要的参数url");
            return false;
//            throw new IllegalArgumentException("缺少必要的参数url");
        }
        return true;
    }

    class Params {
        public String url;
        private String title;
        private String needBack = "0";

        private String userAgent;
        private String needHeader = "0";

        private boolean needScroll = true;

        private String headString = "";

        public String getTitle() {
            return title;
        }

        public String getUserAgent() {
            return userAgent;
        }

        public String getHeadString() {
            return headString;
        }

        public boolean isNeedHeader() {
            return !"0".equals(needHeader);
        }

        public boolean isNeedBack() {
            return !"0".equals(needBack);
        }

        public Params setUrl(String url) {
            this.url = url;
            return this;
        }

        public Params setNeedHeader(String needHeader) {
            this.needHeader = needHeader;
            return this;
        }

        public Params setNeedBack(String needBack) {
            this.needBack = needBack;
            return this;
        }

        public Params setHeadString(String headString) {
            this.headString = TextUtils.isEmpty(headString) ? getHeaderString("", needHeader, needBack) : headString;
            return this;
        }

        public Params setUserAgent(String userAgent) {
            this.userAgent = userAgent;
            return this;
        }

        public boolean isNeedScroll() {
            return needScroll;
        }

        public Params setNeedScroll(boolean needScroll) {
            this.needScroll = needScroll;
            return this;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Params() {
        }

        public Params(Bundle extras) {
            setUrl(extras.getString(H5.URL));
            setNeedHeader(extras.getString(H5.NEED_HEADER, "0"));
            setNeedBack(extras.getString(H5.NEED_BACK, "0"));
            setHeadString(extras.getString(H5.HEADER_STRING, ""));
            setNeedScroll(extras.getBoolean(H5.NEED_SCROLL, true));
            setUserAgent(extras.getString(H5.USER_AGENT, ""));
            setTitle(extras.getString(H5.TITLE, ""));
        }
    }

    interface IBridge {

        Context getContext();

        Params getParams();

        default void requestFocus() {
        }

        @JavascriptInterface
        default void openBrowser(String url) {//H5调用APP打开默认浏览器url: 需要打开的url地址
            if (getContext() == null) return;
            H5.openBrowser(getContext(), url);
        }

        @JavascriptInterface
        default void openWebview(String url, String title) {//title如果有传就显示
            H5.openH5(getContext(), getBaseExtras(title, url));
        }

        @JavascriptInterface
        default void back() {//关闭当前webview
            if (getContext() == null) return;
            Activity activity = (Activity) getContext();
            if (activity != null) {
                activity.finish();
            }
        }

        @JavascriptInterface
        default int getStatusBarHeight() {//获取状态栏高度
            Context context = getContext() == null ? RepositoryComponent.getInstance() : getContext();
            return H5PrivateUtil.getStatusBarHeight(context);
        }

        @JavascriptInterface//放大图片
        default void zoomImg(String src, String index) {
            if (getContext() == null) return;
//            LocalBroadcastManager.getInstance(getContext())
//                    .sendBroadcast(new Intent(HtConstants.Action.SKIP_TO_ZOOM_IMAGE_ACTION)
//                            .putExtra(HtConstants.Params.KEY_LOTTERY_CODE, src)
//                    );
        }

        @JavascriptInterface//放大图片
        default void showToast(String msg) {
            if (getContext() == null) return;
            ToastUtils.showShort(getContext(), msg);
        }

    }
}
