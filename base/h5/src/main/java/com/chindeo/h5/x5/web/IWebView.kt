package com.chindeo.h5.x5.web

import android.view.View
import android.webkit.WebChromeClient.CustomViewCallback

/**
 * Created by zqs on 2022/2/16
 */
interface IWebView {
    /**
     * 显示标题
     */
    fun title(title: String?)

    /**
     * 进度条变化时调用
     *
     * @param newProgress 进度0-100
     */
    fun progress(newProgress: Int)

    fun shouldOverrideUrlLoading(url: String?): Boolean

    fun getVideoLoadingProgressView(): View?

    fun onShowCustomView(view: View?, callback: CustomViewCallback?)

    fun onHideCustomView()
}