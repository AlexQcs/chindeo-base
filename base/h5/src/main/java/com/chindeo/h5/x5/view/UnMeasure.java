package com.chindeo.h5.x5.view;

import android.content.Context;
import android.util.AttributeSet;


public class UnMeasure extends X5WebView {

    public UnMeasure(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnMeasure(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int mExpandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, mExpandSpec);
    }
}