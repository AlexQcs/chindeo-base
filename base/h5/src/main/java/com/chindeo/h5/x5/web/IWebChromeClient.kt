package com.chindeo.h5.x5.web

import android.annotation.TargetApi
import android.app.Activity
import android.net.Uri
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView

/**
 * Created by zqs on 2022/2/16
 */
class IWebChromeClient(val iWebView: IWebView?, val activity: Activity?) : WebChromeClient() {

    var mFilePathCallbacks: ValueCallback<Array<Uri>>? = null

    //标题
    override fun onReceivedTitle(view: WebView?, title: String?) {
        super.onReceivedTitle(view, title)
        if (!TextUtils.isEmpty(title)) {
            iWebView!!.title(title)
        }
    }

    //进度条
    override fun onProgressChanged(view: WebView?, newProgress: Int) {
        super.onProgressChanged(view, newProgress)
        iWebView!!.progress(newProgress)
    }

    //播放视频的相关的几个方法
    override fun getVideoLoadingProgressView(): View? {
        return super.getVideoLoadingProgressView()
    }

    override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
        super.onShowCustomView(view, callback)
        if (view != null && callback != null) {
            iWebView!!.onShowCustomView(view, callback)
        }
    }

    override fun onShowCustomView(
        view: View?,
        requestedOrientation: Int,
        callback: CustomViewCallback?
    ) {
        super.onShowCustomView(view, requestedOrientation, callback)
        if (view != null && callback != null) {
            iWebView!!.onShowCustomView(view, callback)
        }
    }

    override fun onHideCustomView() {
        super.onHideCustomView()
        iWebView!!.onHideCustomView()
    }

    // For Android >= 3.0
    fun openFileChooser(uploadMsg: ValueCallback<Uri?>?, acceptType: String?) {
        this.openFileChooser(uploadMsg, acceptType, null)
        Log.d("WebChromeClient", "openFileChooserAndroid >= >= 3.0")
    }

    // For Android >= 4.1
    fun openFileChooser(uploadMsg: ValueCallback<Uri?>?, acceptType: String?, capture: String?) {
        Log.d("WebChromeClient", "openFileChooserAndroid >= 4.1")
    }

    //Android 5.0及以上用的这个方法 api >= 21
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onShowFileChooser(
        webView: WebView?,
        filePathCallback: ValueCallback<Array<Uri>>?,
        fileChooserParams: FileChooserParams
    ): Boolean {
        mFilePathCallbacks = filePathCallback
        Log.d("WebChromeClient", "openFileChooserAndroid >=Android 5.0")
       /* PictureSelector.create(activity)
            .openGallery(PictureMimeType.ofImage())
            .imageEngine(PictureSelectorEngine.createGlideEngine())
            .maxSelectNum(8)
            .theme(R.style.picture_Sina_style)
            .isWeChatStyle(false)
            .selectionMode(PictureConfig.MULTIPLE)
            .isCamera(true)
            .isPageStrategy(true, true)
            .isCompress(true)
            .forResult(1)*/
        return true
    }
}